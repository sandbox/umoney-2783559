<?php

namespace Drupal\custom_front\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class PathConfigForm.
 *
 * @package Drupal\custom_front\Form
 */
class PathConfigForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'custom_front.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'path_config_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('custom_front.settings');

    $form['anonymous_redirect'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Anonymous redirect'),
      '#description' => $this->t("front page for anonymous user"),
      '#maxlength' => 500,
      '#size' => 64,
      "#default_value" => $config->get('anonymous_redirect'),
    );

    $rolelist='';
    $user_roles = user_role_names();
    foreach ($user_roles as $id=>$name ){
      $rolelist .='<li>'.$id.'=>'.$name.'</li>';
    }

    $settings = '';
    $role_redirect = $config->get('role_redirect');
    foreach ($role_redirect as $role => $path) {
      $settings .= $role . '|' . $path . "\n";
    }

    $form['role_redirect'] = array(
      '#type' => 'textarea',
      '#title' => $this->t('Role based redirect'),
      '#maxlength' => 500,
      '#size' => 64,
      "#default_value" => $settings,
      '#description' => $this->t('A list of role and corresponding path,Enter one pair value per line separated by a pipe:
        Examples:
        <ul>
          <li>finance|/admin/</li>
          <li>staff|/oa/</li>
          <li>sales|/crm/</li>
        </ul>aviable role <ul>'.$rolelist.'</ul>'),
    );

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);
    $config = $this->config('custom_front.settings');
    $config->set('anonymous_redirect',$form_state->getValue('anonymous_redirect'));

    $role_redirect = explode("\n",  $form_state->getValue('role_redirect'));
    $settings = array();
    foreach ($role_redirect as $role) {
      $item = explode("|", $role, 2);
      if (count($item) === 2) {
        $settings[$item[0]] = (isset($settings[$item[0]])) ? $settings[$item[0]] . ' ' : '';
        $settings[$item[0]] .= trim($item[1]);
      }
    }
    $config->set('role_redirect', $settings);
    $config->save();
  }

}
