<?php

namespace Drupal\custom_front\EventSubscriber;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\EventDispatcher\Event;
use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Drupal\Component\Utility\Html;

/**
 * Class PathSubscriber.
 *
 * @package Drupal\custom_front
 */
class PathSubscriber extends ControllerBase implements EventSubscriberInterface {


  /**
   * Constructor.
   */
  public function __construct() {

  }

  /**
   * {@inheritdoc}
   */
  static function getSubscribedEvents() {
    $events['kernel.request'] = ['path_redirect'];

    return $events;
  }

  /**
   * This method is called whenever the kernel.request event is
   * dispatched.
   *
   * @param GetResponseEvent $event
   */
  public function path_redirect(Event $event) {
    if (!\Drupal::service('path.matcher')->isFrontPage()) {
      return ;
    }

    $query = \Drupal::service('request_stack')->getCurrentRequest();
    $uri = $query->getRequestUri();

    $config = $this->config('custom_front.settings');

    $anonymous_redirect = $config->get('anonymous_redirect');
    $currentUser = \Drupal::currentUser();
    $userRoles = $currentUser->getRoles();

    if ( in_array('anonymous', $userRoles) == TRUE) {
      if($this->valid_url($anonymous_redirect)) {
        $event->setResponse(new RedirectResponse($anonymous_redirect));
      }
      return;
    }

    $role_redirect = $config->get('role_redirect');
    foreach ($role_redirect as $role => $path) {
      if ( in_array($role, $userRoles) == TRUE) {
        if($this->valid_url($path)) {
          $event->setResponse(new RedirectResponse($path));
        }
        return;
      }
    }
  }
  function valid_url($url, $absolute = FALSE) {
    if ($absolute) {
      return (bool) preg_match("
      /^                                                      # Start at the beginning of the text
      (?:ftp|https?|feed):\/\/                                # Look for ftp, http, https or feed schemes
      (?:                                                     # Userinfo (optional) which is typically
        (?:(?:[\w\.\-\+!$&'\(\)*\+,;=]|%[0-9a-f]{2})+:)*      # a username or a username and password
        (?:[\w\.\-\+%!$&'\(\)*\+,;=]|%[0-9a-f]{2})+@          # combination
      )?
      (?:
        (?:[a-z0-9\-\.]|%[0-9a-f]{2})+                        # A domain name or a IPv4 address
        |(?:\[(?:[0-9a-f]{0,4}:)*(?:[0-9a-f]{0,4})\])         # or a well formed IPv6 address
      )
      (?::[0-9]+)?                                            # Server port number (optional)
      (?:[\/|\?]
        (?:[\w#!:\.\?\+=&@$'~*,;\/\(\)\[\]\-]|%[0-9a-f]{2})   # The path and query (optional)
      *)?
    $/xi", $url);
    }
    else {
      return (bool) preg_match("/^(?:[\w#!:\.\?\+=&@$'~*,;\/\(\)\[\]\-]|%[0-9a-f]{2})+$/i", $url);
    }
  }
}
