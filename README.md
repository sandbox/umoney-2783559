INTRODUCTION
------------
 custom front module redirect front page according user role. 

INSTALLATION
------------
No special install steps are necessary to use this module, see https://www.drupal.org/documentation/install/modules-themes/modules-8 for further information.

NOTES
-----
Image a B2C site, the guest(anonymous user) will guide to register as comsumer or provider,the comsumer and provider will guide to they own content.
Or, for a OA system ,each department require see it own content.
custom front module provide a setting form to configuate each role's front page.

MAINTAINERS
-----------
Current maintainers:

 * Umoney (https://www.drupal.org/u/umoney)
